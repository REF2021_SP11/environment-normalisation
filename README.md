# Environment Normalisation

REF 2021 SP11 corrected for differences in reviewer Environment Statement scoring behaviour using the Impact Normalisation code (see Impact Normalisation project) applied to each Environment Statement section in turn. Details of the methods used can be found in the Working Methods document in the Documents project.

We hope to be able to provide the real REF 2021 data in anonymised form to allow testing and experimentation, but that has not yet been agreed by the REF Team. If it is not agreed, we will consider generating comparable synthetic data.


